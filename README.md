# Firewall

## Introduction

#### A firewall is a network security device, either hardware or software-based, which monitors all incoming and outgoing traffic and based on a defined set of security rules it accepts, rejects or drops that specific traffic.

```
Accept : allow the traffic
Reject : block the traffic but reply with an “unreachable error”
Drop : block the traffic with no reply
```

## History

#### The first firewalls were developed in the 1980s at the American technology companies Cisco Systems and Digital Equipment Corporation. These “network layer” firewalls judged packets based on simple information such as their apparent source, destination, and connection type.

## Types of Firewall

1. Proxy Firewall
2. Stateful inspection Firewall
3. Unified threat management (UTM) Firewall
4. Next-generation Firewall (NGFW)

## Proxy Firewall

#### A proxy firewall is a network security system that protects network resources by filtering messages at the application layer. A proxy firewall is also be called an application firewall or gateway firewall.

Note :- The Application Layer is the topmost Layer of OSI which provides several ways for manipulating the data (information) which actually enables any type of user to access network with ease.

####

- It is also a proxy server but not vice versa.
- It acts as an intermediary between clients and servers.
- It can cache webpages to reduce bandwidth demands, compress data, filter traffic and detect viruses.
- A proxy server can also be used to hide user information or to connect to services that would be blocked.
- a proxy firewall inspects all network traffic to detect and protect against potential threats
- It can also detect network intrusion and enforce security policies.
- It acts as a gateway between internal users and the internet.
- It can be installed on an organization's network or on a remote server that is accessible by the internal network.
- It provides security to the internal network by monitoring and blocking traffic that is transmitted to and from the internet.

## Stateful inspection firewall

####

- Stateful inspection is the kind of network firewall technology that filters data packets supported by state and context.
- Check Point Software Technologies (CPST) developed the technique within the early 1990s to overcome the restrictions of stateless inspection.

- It detects communications packets over a period of your time and examines both incoming and outgoing packets.
- The firewall follows outgoing packets that request specific sorts of incoming packets and authorize incoming packets to undergo as long as they constitute an accurate response.
- It monitors all sessions and verifies all packets, although the method it uses can vary counting on the firewall technology and therefore the communication protocol getting used.

## Unified threat management (UTM) firewall

####

- A UTM device typically combines, in a loosely coupled way, the functions of a stateful inspection firewall with intrusion prevention and antivirus.
- It may also include additional services and often cloud management.
- UTMs focus on simplicity and ease of use.

## Next-generation Firewall (NGFW)

- Next Generation Firewalls are being deployed these days to stop modern security breaches like advance malware attacks and application-layer attacks.
- NGFW consists of Deep Packet Inspection, Application Inspection, SSL/SSH inspection and many functionalities to protect the network from these modern threats.

### References:

#### https://en.wikipedia.org/wiki/Firewall_(computing)

#### https://www.cisco.com/c/en_in/products/security/firewalls/what-is-a-firewall.html

#### https://www.cisco.com/c/en_in/products/security/firewalls/what-is-a-firewall.html#~types-of-firewalls
